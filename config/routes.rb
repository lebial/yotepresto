Rails.application.routes.draw do
  post 'user_token' => 'user_token#create'
  get 'users/current' => 'users#current'
  get 'users/:id/accounts' => 'users#accounts'
  resources :users

  resources :accounts, only: [:create] do
    member do
      post :deposit
      post :transfer
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
