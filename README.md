#Yo Te Presto Challenge

Este es un proyecto de prueba, a continuacion se detalla la documentacion de como interactuar con el API.

**Tecnologias:**

* Ruby on Rails
* PostgreSQL

**Instrucciones**

* clonar el repo
* cd al directorio de la app
* bundle install
* rails db:create
* rails db:migrate
* rails db:seed
* rails server

***Tambien se puede interactuar con una version en produccion en heroku:***

* https://yo-te-presto-challenge.herokuapp.com/


##API Docs

**Login**

es necesario usar el siguiente email: `admin@example.com` y como password: `12345678` al siguiente url: `url/user_token`.
El usuario anterior es un usuario generado por default.

* Method: Post

```
{
    "auth": {
        "email": "admin@example.com",
        "password": "12345678"
    }
}
```
como resultado obtendremos un token que nos servira para realizar llamadas a otros endpoints.

**Crear Usuario**

* url: `url/users`
* method: `post`
* header: `Authorization` | `Bearer <token generado>`

    ```
    {
      "user": {
        "name": "Pedro",
        "email": "pedro@example.com",
        "password": "dejameEntrar",
        "password_confirmation": "dejameEntrar",
        "admin": "false"
      }
    }
    ```
**nota:** si se pasa false en la bandera `admin` se creara una cuenta automaticamente para dicho usuario.

**Mostrar Usuarios**

* url: `url/users`
* method: `get`
* header: `Authorization` | `Bearer <token generado>`

**Mostrar Cuentas asociadas a un usuario**

* url: `url/users/:id/accounts`
* method: `get`
* header: `Authorization` | `Bearer <token generado>`


**Crear Cuenta Para Usuario**

* url: `url/accounts`
* method: `post`
* header: `Authorization` | `Bearer <token generado>`

    ```
    {
      "name": "holder",
      "user_id": <id de usuario>
    }
    ```

**Depositar A Cuenta De Usuario**

* url: `url/accounts/:id/deposit`
* method: `post`
* header: `Authorization` | `Bearer <token generado>`

    ```
    {
      "amount": <cantidad a depositar>
    }
    ```

**Transferir Entre Cuentas De Usuario**

* url: `url/accounts/:id/transfer`
* method: `post`
* header: `Authorization` | `Bearer <token generado>`

    ```
    {
      "recipient_id": <id de la cuenta que recibira el dinero>
      "amount": <cantidad a depositar>
    }
    ```

**Gracias por visitar!**

