require 'rails_helper'

describe UsersController do
  describe 'POST create' do
    let(:admin_params) { { user: attributes_for(:user, admin: true) } }
    let(:params) { { user: attributes_for(:user) } }
    let!(:admin) { create(:user, admin: true) }
    let(:token) { Knock::AuthToken.new(payload: {sub: admin.id}).token  }

    context 'When user is admin' do
      it 'returns created user id' do
        request.headers['Authorization'] = "Bearer #{token}"
        post :create, params: admin_params
        expect(response_body_as_json['user']['id']).not_to eq nil
      end

      it 'does not creates an account' do
        request.headers['Authorization'] = "Bearer #{token}"
        post :create, params: admin_params
        expect(response_body_as_json['account']).to eq nil
      end
    end

    context 'When user is not an Admin' do
      it 'returns created user id' do
        request.headers['Authorization'] = "Bearer #{token}"
        post :create, params: params
        expect(response_body_as_json['user']['id']).not_to eq nil
      end

      it 'creates an account for the same user' do
        request.headers['Authorization'] = "Bearer #{token}"
        post :create, params: params
        expect(response_body_as_json['account']['id']).not_to eq nil
      end
    end
  end
end
