require 'rails_helper'

describe Account do
  let!(:user) { create(:user) }
  let!(:account) { create(:account, user_id: user.id) }

  # describe '#open' do
  #   context 'with valid params' do
  #     let(:params) { attributes_for(:account, user_id: user.id) }

  #     it 'opens a new account' do
  #       opened = described_class.open(params)
  #       expect(opened).to eq true
  #     end
  #   end

  #   context 'with invalid params' do
  #     let(:without_name) { { user_id: '123b' } }
  #     let(:without_user_id) { { name: 'Foo' } }

  #     it 'validates presence of name attribute' do
  #       expect{described_class.open(without_name)}.to raise_error ActiveRecord::RecordInvalid
  #     end

  #     it 'validates presence of user_id attribute' do
  #       expect{described_class.open(without_user_id)}.to raise_error ActiveRecord::RecordInvalid
  #     end
  #   end
  # end

  describe '#deposit' do
    context 'with valid params' do
      it 'deposit into account' do
        account.deposit(9.99)
        expect(account.balance).to eq 9.99
      end
    end

    context 'when amount <= 0' do
      it 'returns falsy' do
        deposit = account.deposit(0.00)
        expect(deposit).to eq nil
      end
    end
  end

  describe '#withdraw' do
    let!(:cuenta) { create(:account, user_id: user.id, balance: 10.0) }
    context 'with valid params' do
      it 'withdraw from account' do
        cuenta.withdraw(5.00)
        expect(cuenta.balance).to eq 5.0
      end
    end

    context 'when amount <= 0' do
      it 'returns falsy' do
        withdraw = cuenta.withdraw(0.00)
        expect(withdraw).to eq nil
      end
    end
  end

  describe '#transfer' do
    let!(:cuenta) { create(:account, user_id: user.id, balance: 10.0) }
    let!(:user_recipient) { create(:user, email: 'bar@mail.com') }
    let(:params_recipient) { attributes_for(:account, user_id: user_recipient.id ) }
    let!(:recipient) { create(:account, params_recipient) }

    context 'with valid params' do
      it 'transfer from one account to another account' do
        transfer = described_class.transfer(cuenta, recipient, 2.0)
        expect(transfer).to eq true
      end
    end

    context 'when amount <= 0' do
      it 'returns false' do
        transfer = described_class.transfer(cuenta, recipient, 0.00)
        expect(transfer).to eq false
      end
    end
  end
end
