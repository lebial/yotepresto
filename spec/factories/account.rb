FactoryGirl.define do
  factory :account do
    name 'Holder'
    balance 0.00
    user
  end
end
