FactoryGirl.define do
  sequence :email do |n|
    "admin#{n}@example.com"
  end

  sequence :password do |n|
    "1234567#{n}"
  end

  factory :admin do
    email
    password
    admin true
  end
end
