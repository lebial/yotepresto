FactoryGirl.define do
  sequence :mail do |n|
    "user#{n}@example.com"
  end

  sequence :pass do |n|
    "1234567#{n}"
  end

  sequence :name do |n|
    "testy#{n}"
  end

  factory :user do
    name
    email
    password
    admin false
  end
end
