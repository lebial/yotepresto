class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :name
      t.float :balance, default: 0.0
      t.references :user, foreing_key: true

      t.timestamps
    end
  end
end
