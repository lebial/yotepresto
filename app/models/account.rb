class Account < ApplicationRecord
  belongs_to :user
  validates_presence_of :name, :user_id

  def deposit(amount)
    self[:balance] += amount.to_f if self.class.amount_valid?(amount)
  end

  def withdraw(amount)
    self[:balance] -= amount.to_f if self.class.amount_valid?(amount)
  end

  def self.transfer(account, recipient, amount)
    return false unless self.amount_valid?(amount)
    ActiveRecord::Base.transaction do
      self.rollback_error if account.balance < amount
      account.withdraw(amount)
      self.rollback_error unless account.save!
      recipient.deposit(amount)
      self.rollback_error unless recipient.save!
      return true
    end
  end

  private

  def self.amount_valid?(amount)
       return false if amount <= 0
       return true
  end

  def self.rollback_error
    raise ActiveRecord::Rollback
  end
end

