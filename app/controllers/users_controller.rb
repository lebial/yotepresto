class UsersController < ApplicationController
  before_action :set_user, only: [:accounts, :show, :update, :destroy]
  before_action :authenticate_user

  def index
    users = User.all
    render json: users
  end

  def show
    render json: @user
  end

  def create
    if current_user.admin?
      user = User.new(user_params)
      if user.save!
        account = user.accounts.create!(name: 'Holder', user_id: user.id) unless user.admin?
        render json: { user: user, account: account }, status: :created, location: @user
      else
        render json: user.errors, status: :unprocessable_entity
      end
    end
  end

  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @user.destroy
  end

  def accounts
    render json: @user.accounts.all
  end

  def current
    render json: current_user
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :admin)
    end
end
