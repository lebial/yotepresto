class AccountsController < ApplicationController
  before_action :authenticate_user

  def create
    if current_user.admin?
      return head :unprocessable_entity unless account = Account.create!(account_params)
      render json: {account: account, token: refresh_token}, status: :created
    else
      return head :unprocessable_entity
    end
  end

  def deposit
    if current_user.admin?
      account = Account.find(params[:id])
      return head :not_found unless account
      return head :unprocessable_entity unless account.deposit(amount)
      return head :unprocessable_entity unless account.save!
      render json: {deposited: true, token: refresh_token}
    else
      return head :unprocessable_entity
    end
  end

  def transfer
    account = Account.find(params[:id])
    return head :not_found unless account
    recipient_param = params.permit(:recipient_id)
    recipient = Account.find(recipient_param[:recipient_id])
    return head :not_found unless recipient
    if Account.transfer(account, recipient, amount)
      render json: {transfered: true, token: refresh_token}
    else
      return head :unprocessable_entity
    end
  end

  private

  def account_params
    params.require(:account).permit(:name, :user_id)
  end

  def amount
    param = params.permit(:amount)
    param[:amount].to_f
  end

  def refresh_token
    refreshed_token = Knock::AuthToken.new payload: { sub: current_user.id }
    refreshed_token.token
  end
end

